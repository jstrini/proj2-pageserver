# Read Me #
Run a simple Flask server in a Docker container

### Getting Started ###
- Install and setup Docker (See project 0 instructions)
- Clone this repository to your machine
- `docker build -t uocis-flask-demo .`
- `docker run -d -p 5000:5000 UOCIS-flask-demo`
- Launch http://127.0.0.1:5000/ in a web broser
