from flask import Flask
from flask import render_template
from markupsafe import escape
import os

app = Flask(__name__)

@app.route("/")
def hello():
    return "UOCIS docker demo!"

@app.route("/<path:filename>")
def show_file(filename, name=None):
    path = os.path.join(os.getcwd(), "templates/")
    path = os.path.join(path, filename)
    if "//" in path or "~" in path or ".." in path:
        return render_template("403.html"), 403
    elif not os.path.exists(path):
        return render_template("404.html"), 404
    else:
        return render_template(filename), 200

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
